# State Management en Angular con NgRx

Temas

- Conceptos de NgRx & Redux
- Instalación de NgRx
- Declaración y definición de acciones
- Declaración y definición de reductores
- Lectura de datos de NgRx store desde un componente usando selectores
- Envío de acciones con o sin parámetros desde un componente

## Preparación del proyecto base

- Creación de un directorio para el nuevo proyecto
- $ cd `/directorio/de/proyectos`
- $ mkdir ngrx-todo
- $ cd ngrx-todo
- $ git clone https://gitlab.com/j_sauceda/angular-todo.git .
- Instalar dependencias: $ npm install
- Probar que el código se ejecuta: $ ng serve

## Instalar y configurar NgRx

- Para mayor información, leer [la documentación oficial](https://ngrx.io/guide/store), [este artículo en LogRocket](https://blog.logrocket.com/angular-state-management-made-simple-with-ngrx/) o ver [este tutorial de 30 minutos en Youtube](https://www.youtube.com/watch?v=SkoI_VHtcTU)
- $ npm install @ngrx/store
- $ npm install @ngrx/store-devtools
- Crear directorio para ngrx store en /src/app: $ mkdir store
- Definir ngrx actions: $ touch src/app/store/item.actions.ts:

```javascript
import { createAction, props } from '@ngrx/store';

export const ADD_ITEM = createAction(
  '[App Item] Create item',
  props<{ description: string; done: boolean }>()
);

export const DELETE_ITEM = createAction(
  '[App Item] Delete item',
  props<{ id: number }>()
);

export const EDIT_DESCRIPTION = createAction(
  '[App Item] Edit item description',
  props<{ id: number; new_description: string }>()
);

export const CHANGE_STATUS = createAction(
  '[App Item] Change item done status',
  props<{ id: number; done: boolean }>()
);
```

- Definir interfaz de estado: $ touch src/app/store/item-state.interface.ts:

```javascript
import Item from 'src/app/shared/item.model';

export interface ItemsState {
  items: Array<Item>;
}
```

- Definir ngrx reducer: $ touch src/app/store/item.reducer.ts:

```javascript
import { createReducer, on } from '@ngrx/store';
import Item from '../shared/item.model';

import {
  ADD_ITEM,
  DELETE_ITEM,
  EDIT_DESCRIPTION,
  CHANGE_STATUS,
} from './item.actions';

export const initialState: Array<Item> = [
  { description: 'Despertar', done: true },
  { description: 'Desayunar', done: true },
];

export const itemReducer = createReducer(
  initialState,
  on(
    ADD_ITEM,
    (state, { description, done }) => [
      ...state,
      { description: description, done: done },
    ]
    // next statement wouldn't work due to state immutability
    // state.push({ description: description, done: done })
  ),
  on(
    DELETE_ITEM,
    (state, { id }) => state.filter((item, index) => index !== id)
    // next statement wouldn't work due to state immutability
    // state.splice(id, 1)
  ),
  on(
    EDIT_DESCRIPTION,
    (state, { id, new_description }) => [
      ...state.slice(0, id),
      { description: new_description, done: state[id].done },
      ...state.slice(id + 1),
    ]
    // next statement wouldn't work due to state immutability
    // state.filter((item, index) =>
    //   index === id ? { description: new_description, done: item.done } : item
    // )
  ),
  on(
    CHANGE_STATUS,
    (state, { id, done }) => [
      ...state.slice(0, id),
      { description: state[id].description, done: done },
      ...state.slice(id + 1),
    ]
    // next statement wouldn't work due to state immutability
    // state.filter((item, index) =>
    //   index === id ? { description: item.description, done: done } : item
    // )
  )
);
```

- Editar app.module.ts (importar módulos y agregar imports):

```javascript
import { NgModule, isDevMode } from '@angular/core';
...
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { itemReducer } from './store/item.reducer';
...
@NgModule({
  imports: [
    BrowserModule,
    StoreModule.forRoot({
      // count: counterReducer,
      items: itemReducer,
    }),
    StoreDevtoolsModule.instrument({
      maxAge: 10, // Retains last 25 states
      logOnly: !isDevMode(), // Restrict extension to log-only mode
      autoPause: true, // Pauses recording actions and state changes when the extension window is not open
      trace: true, //  If set to true, will include stack trace for every dispatched action, so you can see it in trace tab jumping directly to that part of code
      traceLimit: 75, // maximum stack trace frames to be stored (in case trace option was provided as true)
    }),
  ],
  ...
})
...
```

## Integrar NgRx store con los componentes

- Editar app.component.ts:

```javascript
...
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { ItemsState } from './store/item-state.interface';
import {
  ADD_ITEM,
  DELETE_ITEM,
  EDIT_DESCRIPTION,
  CHANGE_STATUS,
} from './store/item.actions';
...
export class AppComponent {
  ...
  items$: Observable<Array<Item>>;

  constructor(private store: Store<ItemsState>) {
    this.items$ = store.select((store) => store.items);
  }

  get items() {
    if (this.filters === this.appFilter.all) {
      return this.allItems;
      // next statement would work in readonly mode due to state immutability
      // return this.items$;
    }
    return this.allItems.filter((item) =>
      this.filters === this.appFilter.done ? item.done : !item.done
    );
    // next statement would work in readonly mode due to state immutability
    // return this.items$.subscribe((items) => {
    //   items.filter((item) =>
    //     this.filters === this.appFilter.done ? item.done : !item.done
    //   );
    // });
  }

  addItem(new_description: string) {
    if (!new_description) return;
    this.allItems.push({
      description: new_description,
      done: false,
    });
    this.store.dispatch(
      ADD_ITEM({
        description: new_description,
        done: false,
      })
    );
  }

  changeStatus(done: boolean, id: number) {
    this.allItems[id].done = done;
    this.store.dispatch(CHANGE_STATUS({ done, id }));
  }

  updateDescription(new_description: string, id: number) {
    this.allItems[id].description = new_description;
    this.store.dispatch(EDIT_DESCRIPTION({ new_description, id }));
  }

  removeItem(item: Item, index: number) {
    this.allItems.splice(this.allItems.indexOf(item), 1);
    this.store.dispatch(
      DELETE_ITEM({
        id: index,
      })
    );
  }
}
```

- Editar app.component.html:

```xml
<div class="container">
  <h1>NgRx TODO list</h1>

  <!-- <div>
    current items:
    <ul>
      <li *ngFor="let i of items$ | async">item: {{ i | json }}</li>
    </ul>
  </div> -->

  ...

  <ul class="list-group">
    <!-- <li
      *ngFor="let item of items$ | async; let i = index"
      class="list-group-item mb-1"
    > -->
    <li *ngFor="let item of items; let i = index" class="list-group-item mb-1">
      <app-item
        [item]="item"
        (changeStatus)="changeStatus(item.done, i)"
        (updateDescription)="updateDescription(item.description, i)"
        (remove)="removeItem(item, i)"
      ></app-item>
    </li>
  </ul>
</div>
```


import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { Filter } from './shared/Filter';
import Item from './shared/item.model';

import { ItemsState } from './store/item-state.interface';
import {
  ADD_ITEM,
  DELETE_ITEM,
  EDIT_DESCRIPTION,
  CHANGE_STATUS,
} from './store/item.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'Angular Todo';
  appFilter = Filter;

  filters = this.appFilter.all;

  allItems: Item[] = [
    { description: 'Despertar', done: true },
    { description: 'Desayunar', done: true },
  ];

  items$: Observable<Array<Item>>;

  constructor(private store: Store<ItemsState>) {
    this.items$ = store.select((store) => store.items);
  }

  get items() {
    if (this.filters === this.appFilter.all) {
      return this.allItems;
      // next statement would work in readonly mode due to state immutability
      // return this.items$;
    }
    return this.allItems.filter((item) =>
      this.filters === this.appFilter.done ? item.done : !item.done
    );
    // next statement would work in readonly mode due to state immutability
    // return this.items$.subscribe((items) => {
    //   items.filter((item) =>
    //     this.filters === this.appFilter.done ? item.done : !item.done
    //   );
    // });
  }

  addItem(new_description: string) {
    if (!new_description) return;
    this.allItems.push({
      description: new_description,
      done: false,
    });
    this.store.dispatch(
      ADD_ITEM({
        description: new_description,
        done: false,
      })
    );
  }

  changeStatus(done: boolean, id: number) {
    this.allItems[id].done = done;
    this.store.dispatch(CHANGE_STATUS({ done, id }));
  }

  updateDescription(new_description: string, id: number) {
    this.allItems[id].description = new_description;
    this.store.dispatch(EDIT_DESCRIPTION({ new_description, id }));
  }

  removeItem(item: Item, index: number) {
    this.allItems.splice(this.allItems.indexOf(item), 1);
    this.store.dispatch(
      DELETE_ITEM({
        id: index,
      })
    );
  }
}

import { createAction, props } from '@ngrx/store';

export const ADD_ITEM = createAction(
  '[App Item] Create item',
  props<{ description: string; done: boolean }>()
);

export const DELETE_ITEM = createAction(
  '[App Item] Delete item',
  props<{ id: number }>()
);

export const EDIT_DESCRIPTION = createAction(
  '[App Item] Edit item description',
  props<{ id: number; new_description: string }>()
);

export const CHANGE_STATUS = createAction(
  '[App Item] Change item done status',
  props<{ id: number; done: boolean }>()
);

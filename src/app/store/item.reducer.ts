import { createReducer, on } from '@ngrx/store';
import Item from '../shared/item.model';

import {
  ADD_ITEM,
  DELETE_ITEM,
  EDIT_DESCRIPTION,
  CHANGE_STATUS,
} from './item.actions';

export const initialState: Array<Item> = [
  { description: 'Despertar', done: true },
  { description: 'Desayunar', done: true },
];

export const itemReducer = createReducer(
  initialState,
  on(
    ADD_ITEM,
    (state, { description, done }) => [
      ...state,
      { description: description, done: done },
    ]
    // next statement wouldn't work due to state immutability
    // state.push({ description: description, done: done })
  ),
  on(
    DELETE_ITEM,
    (state, { id }) => state.filter((item, index) => index !== id)
    // next statement wouldn't work due to state immutability
    // state.splice(id, 1)
  ),
  on(
    EDIT_DESCRIPTION,
    (state, { id, new_description }) => [
      ...state.slice(0, id),
      { description: new_description, done: state[id].done },
      ...state.slice(id + 1),
    ]
    // next statement wouldn't work due to state immutability
    // state.filter((item, index) =>
    //   index === id ? { description: new_description, done: item.done } : item
    // )
  ),
  on(
    CHANGE_STATUS,
    (state, { id, done }) => [
      ...state.slice(0, id),
      { description: state[id].description, done: done },
      ...state.slice(id + 1),
    ]
    // next statement wouldn't work due to state immutability
    // state.filter((item, index) =>
    //   index === id ? { description: item.description, done: done } : item
    // )
  )
);

import Item from 'src/app/shared/item.model';

export interface ItemsState {
  items: Array<Item>;
}
